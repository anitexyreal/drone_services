package com.titicodes.drone_service.repository;


import java.util.List;


import com.titicodes.drone_service.data.Drone;
import com.titicodes.drone_service.utils.DroneState;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DroneRepository extends CrudRepository<Drone,String> {

    Drone findBySerialNumber(String serialNumber);
    List<Drone> findAll();

    List<Drone> findByDroneState(DroneState state);

}

