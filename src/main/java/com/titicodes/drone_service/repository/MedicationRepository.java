package com.titicodes.drone_service.repository;
import java.util.List;

import com.titicodes.drone_service.data.Medication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MedicationRepository extends CrudRepository<Medication,Long>{

    List<Medication> findAll();
    List<Medication> findAllByIdIn(List<Long> ids);

}
