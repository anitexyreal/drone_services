package com.titicodes.drone_service.repository;


import java.util.List;


import com.titicodes.drone_service.data.DroneActivity;
import com.titicodes.drone_service.utils.DroneState;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DroneActivityRepository extends CrudRepository<DroneActivity,Long> {

    List<DroneActivity> findByDroneIdAndStateIn(String droneId, List<DroneState> states);
}

