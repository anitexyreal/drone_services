package com.titicodes.drone_service.utils;

public enum DroneModel {

    Lightweight, Middleweight, Cruiserweight, Heavyweight

}
