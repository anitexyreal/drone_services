package com.titicodes.drone_service.config;

import com.titicodes.drone_service.data.mapper.DroneMapper;
import com.titicodes.drone_service.repository.DroneRepository;
import com.titicodes.drone_service.service.DroneActivityService;
import com.titicodes.drone_service.service.DroneService;
import com.titicodes.drone_service.service.MedicationService;
import com.titicodes.drone_service.service.impl.DroneActivityServiceImpl;
import com.titicodes.drone_service.service.impl.DroneServiceImpl;
import com.titicodes.drone_service.service.impl.MedicationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Autowired
    private DroneRepository droneRepo;

    @Autowired
    private DroneMapper droneMapper;
    @Bean
    public DroneService droneService() {
        return new DroneServiceImpl(droneRepo,droneMapper);
    }

    @Bean
    public MedicationService medicationService() {
        return new MedicationServiceImpl();
    }

    @Bean
    public DroneActivityService activityService() {
        return new DroneActivityServiceImpl(medicationService());
    }
}
