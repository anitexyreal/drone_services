package com.titicodes.drone_service.data.mapper;


import java.util.List;

import com.titicodes.drone_service.data.Drone;
import com.titicodes.drone_service.data.dto.AvailableDronesDTO;
import com.titicodes.drone_service.data.dto.DroneBatteryDTO;
import com.titicodes.drone_service.data.dto.DroneDTO;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel="spring")
public interface DroneMapper {

    DroneMapper INSTANCE = Mappers.getMapper(DroneMapper.class);


    @Mapping(target="weightLoaded",source="drone.currentWeight")
    DroneDTO modelToDto(Drone drone);

    List<DroneDTO> dronesToDto(List<Drone> drones);

    List<DroneBatteryDTO> dronesToBatteryLog(List<Drone> drones);


    @InheritInverseConfiguration
    Drone dtoToDrone(DroneDTO dto);

    @Mapping(target="serialNumber",source="drone.serialNumber")
    @Mapping(target="model",source="drone.droneModel")
    @Mapping(target="batteryLife",source="drone.batteryLife")
    DroneBatteryDTO droneToBetteryDto(Drone drone);

    @Mapping(target="total",source="total")
    @Mapping(target="drones",source="availableDrones")
    AvailableDronesDTO dronesToAvailableDTO(int total, List<Drone> availableDrones);

}

