package com.titicodes.drone_service.data.mapper;
import java.util.List;

import com.titicodes.drone_service.data.Medication;
import com.titicodes.drone_service.data.dto.MedicationDTO;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel="spring")
public interface MedicationMapper {

    MedicationMapper INSTANCE = Mappers.getMapper(MedicationMapper.class);

    MedicationDTO medicationToDTOo(Medication medication);

    @InheritInverseConfiguration
    Medication dtoToMedication(MedicationDTO dto);

    @InheritInverseConfiguration
    List<Medication> dtosToMedications(List<MedicationDTO> dtos);

}
