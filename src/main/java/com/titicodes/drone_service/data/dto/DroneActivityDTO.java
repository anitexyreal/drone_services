package com.titicodes.drone_service.data.dto;

import java.util.List;

import lombok.Data;

/**
 *
 * @author Anietimfon Effiong
 * DTO class for displaying a drone's activity and loaded medications
 *
 */
public @Data class DroneActivityDTO {

    private int total;
    private List<LoadDroneDTO> activities;
}

