package com.titicodes.drone_service.data.dto;


import java.util.List;

import lombok.Data;
/**
 *
 * @author Anietimfon Effiong
 * DTO class for displaying drones available for loading
 *
 */
public @Data class AvailableDronesDTO {

    private int total;
    private List<DroneDTO> drones;

}

