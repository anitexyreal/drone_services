package com.titicodes.drone_service.controller;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.titicodes.drone_service.data.Drone;
import com.titicodes.drone_service.data.Medication;
import com.titicodes.drone_service.data.dto.DroneActivityDTO;
import com.titicodes.drone_service.data.dto.LoadDroneDTO;
import com.titicodes.drone_service.service.DroneActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping(path = {"/drones/activity"}, produces = APPLICATION_JSON_VALUE)
public class DroneActivityController {


    @Autowired
    private DroneActivityService activityService;


    @PostMapping(value="/load",consumes=APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> loadDrone(@Valid @RequestBody LoadDroneDTO dto){
        Response res = activityService.loadDrone(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }



    @GetMapping("/check/{serialNumber}")
    public ResponseEntity<DroneActivityDTO> findAvailableDroneActivity(@PathVariable("serialNumber")String ref){

        DroneActivityDTO dto = activityService.getDroneActivity(ref);
        if(dto!=null)
            return ResponseEntity.ok(dto);
        else
            return ResponseEntity.notFound().build();
    }


}
