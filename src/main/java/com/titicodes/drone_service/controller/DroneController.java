package com.titicodes.drone_service.controller;


import com.titicodes.drone_service.data.dto.AuditDTO;
import com.titicodes.drone_service.data.dto.AvailableDronesDTO;
import com.titicodes.drone_service.data.dto.DroneBatteryDTO;
import com.titicodes.drone_service.data.dto.DroneDTO;
import com.titicodes.drone_service.service.DroneService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author Anietimfon Effiong
 * This Drone Controller will expose the following services
 * 1. Register a new drone
 * 2. Check drone battery life
 * 3. Check available drones for loading
 * 4. Get drone audit
 *
 */

@RestController
@RequestMapping(path = {"/drones"}, produces = APPLICATION_JSON_VALUE)

public class DroneController {

    @Autowired
    private DroneService droneService;




    @PostMapping(value="/register",consumes=APPLICATION_JSON_VALUE)
    public ResponseEntity<DroneDTO> registerDrone(@Valid @RequestBody DroneDTO dto){

        DroneDTO _dto = droneService.registerDrone(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(_dto);
    }



    @GetMapping(path = "/{serialNumber}")
    public ResponseEntity<DroneBatteryDTO> getDroneBattery(@PathVariable("serialNumber")String serialNumber){

        DroneBatteryDTO dto = droneService.checkBattery(serialNumber);
        if(dto!=null) {
            return ResponseEntity.ok(dto);
        }
        else
            return ResponseEntity.notFound().build();

    }


    @GetMapping("/available")
    public ResponseEntity<AvailableDronesDTO> findAvailableDrones(){

        AvailableDronesDTO dto = droneService.listAvailableDrones();
        return ResponseEntity.ok(dto);
    }



    @GetMapping("/audit")
    public ResponseEntity<AuditDTO> getDroneAudit(){
        AuditDTO audit = droneService.getDroneAudit();
        return ResponseEntity.ok(audit);

    }





}
