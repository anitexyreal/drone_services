package com.titicodes.drone_service.controller;



import lombok.Data;

public @Data class Response {

    private String data;
    private String status;

}

