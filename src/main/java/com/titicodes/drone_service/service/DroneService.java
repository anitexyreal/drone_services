package com.titicodes.drone_service.service;


import com.titicodes.drone_service.data.dto.AuditDTO;
import com.titicodes.drone_service.data.dto.AvailableDronesDTO;
import com.titicodes.drone_service.data.dto.DroneBatteryDTO;
import com.titicodes.drone_service.data.dto.DroneDTO;

public interface DroneService {

    public DroneDTO registerDrone(DroneDTO droneDto);
    public DroneDTO getDrone(String serialNumber);
    public AuditDTO getDroneAudit();
    public AvailableDronesDTO listAvailableDrones();
    public DroneBatteryDTO checkBattery(String serialNumber);

}

