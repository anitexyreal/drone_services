package com.titicodes.drone_service.service;


import com.titicodes.drone_service.controller.Response;
import com.titicodes.drone_service.data.dto.DroneActivityDTO;
import com.titicodes.drone_service.data.dto.LoadDroneDTO;

public interface DroneActivityService {

    public Response loadDrone(LoadDroneDTO drontActivityDto);
    public DroneActivityDTO getDroneActivity(String serialNumber);

}
