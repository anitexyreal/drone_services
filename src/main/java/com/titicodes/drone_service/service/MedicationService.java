package com.titicodes.drone_service.service;


import com.titicodes.drone_service.data.Medication;
import com.titicodes.drone_service.data.dto.MedicationDTO;

import java.util.List;



public interface MedicationService {

    public void createMedication(MedicationDTO medDto);
    public List<Medication> createMedication(List<MedicationDTO> medDtos);
    public List<Medication> listMedications();
    public List<Medication> listMedications(List<Long> ids);

}

